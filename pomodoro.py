import time
import argparse
import sys

MINUTE = 60


def check_os():
    platform = sys.platform
    match platform:
        case "linux":
            return notify_linux
        case "darwin":
            return notify_mac
        case "win32":
            return notify_windows


def timer(duration: int):
    wait = duration * MINUTE
    time.sleep(wait)


def notify_linux(message: str):
    import subprocess
    subprocess.call(['notify-send', 'Pomodoro Timer for Linux', message])


def notify_mac(message: str):
    import pync
    pync.notify(message, title="Pomodoro Timer for MacOS")


def notify_windows(message: str):
    from win10toast import ToastNotifier
    toaster = ToastNotifier()
    toaster.show_toast(message, "Pomodoro timer for Windows", threaded=True, icon_path=None, duration=5)


def main():
    parser = argparse.ArgumentParser(description="Optional information for pomodoro timer")
    parser.add_argument("--duration", type=int, help="How long the Pomodoro session should last in whole minutes",
                        required=False)
    parser.add_argument("--message", type=str, help="Message to be displayed in pop up notification", required=False)
    args = parser.parse_args()
    duration = args.duration or 25
    message = args.message or "Time's up!"
    timer(duration=duration)
    notify = check_os()
    notify(message=message)


if __name__ == "__main__":
    main()
