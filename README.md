# <center> Domodoro, an open source, cross platform pomodoro script </center>

Given that I couldn't find an open source, libre app to use for pomodoro on MacOS in five minutes, I decided to write one. Then I made it cross platform.


## How to use

### Installation

Prerequisites: 
- Python 3.6 or higher (It might work with older versions, but it's untested)
- git installed


Getting the code:
```
git clone git@gitlab.com:Dom_YSK/domodoro.git
cd domodoro
```
Next part is currently OS specific. If I add a GUI then I'll probably just write an installer. 

Windows:
```
python -m venv .venv
.venv\Scripts\activate
python -m pip install -r requirements.txt
```

Linux/MacOS:
```
python3 -m venv .venv
source .venv\bin\activate
python -m pip install -r requirements.txt
```


### Usage

There's currently 2 arguments that the script takes duration and message. 


`--duration`: 

This defaults to 25 minutes, it accepts integers only and is always in minutes. 


`--message`: 

This defaults to "Time's Up" but will accept any string. If you break your machine with a very long and/or malicious string, that's on you. 


Example:

`python pomodoro.py --duration=5 --message="Your timer is finished!"`

You can pull up the help by passing the `--help` flag 
